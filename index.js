const name = prompt("Your name?");
alert(`Hello, ${name}`);

const age = prompt("Your age?");
alert(`Your age is ${age}`);

if (age < 18) {
  alert("You to young for this website!");
}

if (age > 18 && age <= 22) {
  confirm("Are you sure you want to continue?");
}

const result = confirm ('Are you sure you want to continue?');

if (result) {
    alert(`Welcome, ${name}`);
  } else {
    alert('You are not allowed to visit this website');
  }










// Теоритечиские вопросы HW-2

//1//

// Примитивные типы данных в JS:
// // 1. Undefined
// typeof undefined === 'undefined'

// // 2. Boolean, логический
// typeof true === 'boolean'
// typeof false === 'boolean'

// // 3. Number, число
// typeof 42 === 'number'
// typeof 4.2 === 'number'
// typeof -42 === 'number'
// typeof Infinity === 'number'
// typeof -Infinity === 'number'

// // 4. String, строка
// typeof '' === 'string'
// typeof 'string' === 'string'
// typeof 'number' === 'string'
// typeof 'boolean' === 'string'

// // 5. Symbol, символ, ES6
// typeof Symbol() === 'symbol'

// // 6. BigInt, большое число, ES6
// typeof 9007199254740991n === 'bigint'
// typeof BigInt(9007199254740991) === 'bigint'

// // 7. Null
// typeof null === 'object'- баг в JS

// Также есть не примитивные типы данных, например объекты и массивы.

//2//

// ==-нестрогое сравнение. Допукскает преобразование типа при проверке равенства.JS приводит типы самостоятельно
// ===-строгое сравнение. Запрещает преобразование типов. При строгом сравнении интерпретатор учитывает типы сравниваемых значений.

// console.log(5 == '5')
// // true
// console.log(5 === '5')
// // false

//3//

// Оператор – это внутренняя функция JavaScript.
// При использовании того или иного оператора мы запускаем ту или иную встроенную функцию,
// которая выполняет какие-то определённые действия и возвращает результат.
//  Логические && || !, условные if, else, else if.
